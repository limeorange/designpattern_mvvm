package com.sallysoft.testviewbinding;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;

import android.os.Bundle;
import android.widget.Button;

import com.sallysoft.testviewbinding.databinding.ActivityMainBinding;
import com.sallysoft.testviewbinding.ui.main.BlankFragment;
import com.sallysoft.testviewbinding.ui.main.MainViewModel;

import androidx.viewpager2.widget.ViewPager2;

public class MainActivity extends AppCompatActivity {
    private ActivityMainBinding activityMainBinding;

    private BlankFragment[] fragments = {BlankFragment.newInstance(1), BlankFragment.newInstance(2), BlankFragment.newInstance(3)};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityMainBinding = ActivityMainBinding.inflate(getLayoutInflater());
        setContentView(activityMainBinding.getRoot());
        MainViewModel viewModel = new ViewModelProvider(this).get(MainViewModel.class);
        setViewPager();
        activityMainBinding.mainButton.setOnClickListener(view->{viewModel.add();});

    }

    private void setViewPager()
    {
        ViewPager2 viewPager2 = (ViewPager2) findViewById(R.id.viewpager);

        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager(), getLifecycle());
        adapter.setList(fragments);
        viewPager2.setAdapter(adapter);
    }

}