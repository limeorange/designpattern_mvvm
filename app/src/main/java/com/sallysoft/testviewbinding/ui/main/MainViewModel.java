package com.sallysoft.testviewbinding.ui.main;

import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class MainViewModel extends ViewModel {
    // TODO: Implement the ViewModel
    private MutableLiveData<Integer> data = null ;//new MutableLiveData(0);

    public MainViewModel(){
        if(data == null) {
            data = new MutableLiveData(0);
            Log.i("MainViewModel -getMutabledata", data.getValue().toString());
        }
    }

    //data binding시 필요
    public LiveData<Integer> getMutableData()
    {
        if(data == null) {
            data = new MutableLiveData(0);
        }

        Log.i("MainViewModel -getMutabledata", data.getValue().toString());
        return data;
    }

    //값 증가
    public void add()
    {
        data.setValue(data.getValue()+1);
        Log.i("MainViewModel", data.getValue().toString());
    }
}